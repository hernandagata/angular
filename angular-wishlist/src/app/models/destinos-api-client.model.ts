import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store, State } from '@ngrx/store';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
    destinos: DestinoViaje[] = [];
    //destinos:DestinoViaje[];
    //current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);

    constructor(private store: Store<AppState>, 
        @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
        private http: HttpClient) {
        //this.des(tinos = [];
        this.store
        .select(state => state.destinos)
        .subscribe((data) => {
            console.log('destinos usb store');
            console.log(data);
            this.destinos = data.items;
        });
        this.store
        .subscribe((data) => {
            console.log('all store');
            console.log(data);
        });
    }

    add(d: DestinoViaje) {
        //this.destinos.push(d);
        //this.store.dispatch(new NuevoDestinoAction(d));
        const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
        const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', {nuevo: d.nombre}, {headers: headers });
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            if (data.status === 200) {
                this.store.dispatch(new NuevoDestinoAction(d));
                const myDb = db;
                myDb.destinos.add(d);
                console.log('todos los destinos de la db');
                myDb.destinos.toArray().then(destinos => console.log(destinos))
            }
        });
    }

    getById(id: String): DestinoViaje {
        return this.destinos.filter(function(d) {return d.id.toString === id; })[0];
    }

    getAll(): DestinoViaje[] {
      return this.destinos;
     }

    elegir(d: DestinoViaje) {
        //this.destinos.forEach(x => x.setSelected(false));
        this.store.dispatch(new ElegidoFavoritoAction(d));
        //d.setSelected(true);
        //this.current.next(d);
    }

    //subcribeOnchange(fn) {
    //    this.current.subscribe(fn);
    //}
}