import {v4 as uuid} from 'uuid';

export class DestinoViaje {
    public selected: boolean;
    public servicios: string[];
    id: any;
    constructor(public nombre: string, public imagenUrl: string, public votes: number = 0){
        this.servicios = ['piscina','desayuno'];
    }

    setSelected(s: boolean) {
        this.selected = s;
    }

    isSelected(): boolean {
        return this.selected;
    }

    voteUp() {
        this.votes++;
    }

    voteDwon() {
        this.votes--;
    }
    
}
