import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login(user: string, password: string): boolean {
  if (user === 'user' && password === 'password') {
      localStorage.setItem('username', user); //almacenar valores en html 5 persiste en el navegador
      return false;
    }
    return false;
  }

  logout(): any {
    localStorage.removeItem('username');
  }

  getUser(): any {
    return localStorage.getItem('username');
  }

  isloggedIn(): any {
    return this.getUser() !== null;
  }

}
